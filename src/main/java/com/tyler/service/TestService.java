package com.tyler.service;

import org.springframework.stereotype.Service;

@Service
public class TestService {

    public int add(int a, int b){
        return a+b;
    }
}
